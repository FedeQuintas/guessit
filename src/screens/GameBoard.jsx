import React from 'react';
import styled from 'styled-components';
import Timer from './Timer';
import AnswerResultMessage from '../screens/AnswerResultMessage'
import provideApiGateway from '../infrastructure/ApiGateway.js'
import provideSubmitAnswer from '../domain/actions/SubmitAnswer.js'

const FactsAndInputContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 480px;
`;

const Facts = styled.div`
  flex:3;
  margin-left: 15px;
  margin-right: 15px;
`;

const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: center;
`;

const NameInputLabel = styled.div`
  font-size: 0.8rem;
  align-self: center;
`;

const InputText = styled.input`
  color: palevioletred;
  font-size: 1rem;
  border: 2px solid palevioletred;
  border-radius: 3px;
  height: 1rem;
  text-align: center;
  padding: 10px;
  width: 60%;
  align-self: center;
  font-family: monospace;
`;

const SendGuessButton = styled.button`
  font-size: 1rem;
  width: 50%;
  align-self: center;
  border-radius: 1rem;
  color: black;
  margin-top: 10px;
  font-family: monospace;
  border:solid 2px #41403E;
  box-shadow:2px 8px 4px -6px;
  background:transparent;
  letter-spacing:1px;
  box-shadow: 20px 38px 34px -26px 
  margin-top: 20px;
`;

const Fact = styled.div`
  font-size: 1rem;
  padding: 8px;
  border-radius: 1rem;
  border:solid 2px #41403E;
  margin-top: 10px;
  text-align: center;
  justify-content: center;
  margin-top:10px;
`;

class GameBoard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      userInputValue: '',
      currentCount: props.timeToAnswer,
      showTimer: true,
      showResult: false,
      correctAnswer: false,
      score:0,
      facts: []
    };
    this.apiGateway = provideApiGateway();
    this.submitAnswer = provideSubmitAnswer();
  }

  feedback = (hasAnsweredCorrectly) => {
    var newScore = this.state.score;
    if(hasAnsweredCorrectly){
      newScore = newScore + 1;
    }
    this.setState({
      hasAnsweredCorrectly,
      showResult: true,
      showTimer: false,
      score: newScore,
    });
  }

  refreshFacts = (facts, character) => {
    this.setState(
      {facts,
       character,  
       showResult: false,
       showTimer: true,
       currentCount : this.props.timeToAnswer,
       userInputValue: '',
      });
    }
    
  handleSubmit = (event) => {
    this.submitAnswer(this.state.userInputValue.toLowerCase(), this.state.character, this.feedback, this.refreshFacts);
  }

  showNewFacts = () => {
    return this.apiGateway.getNewFacts().then(data => {
        this.setState({facts : data.characterFacts.facts, character: {name: data.characterFacts.character, id: data.characterFacts.id}})
        clearInterval(this.intervalId);
      }).catch(ex => {
        throw ex;
    });
  }

  handleChange = ({ target }) => {
    this.setState({
      [target.name]: target.value
    });
  }

  componentDidMount = () => {
    this.showNewFacts();
    setInterval(() => {
      if(this.state.currentCount > 0)
      this.setState({currentCount : this.state.currentCount - 1})
    }, 1000);
  }

  playAgain = () => {
    this.setState({hasAnsweredCorrectly: true,
                   score: 0,
                   userInputValue: '',
                   showResult: false,
                   showTimer: true,
                   currentCount : this.props.timeToAnswer,
                  });
    this.showNewFacts();
  }

  isIncorrectAnswer = () => {
    return (this.state.hasAnsweredCorrectly !== undefined && !this.state.hasAnsweredCorrectly) || this.state.currentCount < 1;
  }

  render() {
    if (this.isIncorrectAnswer()){
      return <AnswerResultMessage currentCount={this.state.currentCount} showTimer={this.state.showTimer} hasAnsweredCorrectly={false} showResult={true} score={this.state.score} onPlayAgain={this.playAgain}/>
    }
    return (     
        <FactsAndInputContainer>
          <Facts>
          <Timer currentCount={this.state.currentCount} showTimer={this.state.showTimer}/>
            {this.state.facts.map(fact =>
           
            <Fact key={fact}> 
              {fact}
            </Fact>
            )} 
          </Facts>
          <AnswerResultMessage currentCount={this.state.currentCount} showTimer={this.state.showTimer} hasAnsweredCorrectly={this.state.hasAnsweredCorrectly} showResult={this.state.showResult} score={this.state.score}/>
          <InputWrapper>
            <NameInputLabel/>         
            <InputText
              type="text" 
              name="userInputValue" 
              placeholder="here you could write" 
              value={this.state.userInputValue}
              onChange={this.handleChange} 
            />
            <SendGuessButton onClick={this.handleSubmit}>
              guess it bro
            </SendGuessButton>
          </InputWrapper>
        </FactsAndInputContainer>
    );
  }
}

export default GameBoard;
