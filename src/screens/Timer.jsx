import React from 'react';
import styled from 'styled-components';

const TimerText = styled.p`
  font-size: 1.1rem;
  font-family: monospace;
  margin-left: 275px;
`;


class Timer extends React.Component {

  render() {
    return (  
      <div>
       <TimerText>{this.props.showTimer ? this.props.currentCount + '"': ''}</TimerText>
     </div>
    );
  }
}


export default Timer;
