import React from 'react';
import styled from 'styled-components';

const AnswerResult = styled.p`

`;

const Message = styled.div`
  font-size: 2rem;
  font-family: monospace;
  text-align: center;
  padding-top: 80px;
`;

const PlayAgainButton = styled.button`
  font-size: 1rem;
  width: 50%;
  align-self: center;
  border-radius: 1rem;
  color: black;
  margin-top: 10px;
  font-family: monospace;
  border:solid 2px #41403E;
  box-shadow:2px 8px 4px -6px;
  background:transparent;
  letter-spacing:1px;
  box-shadow: 20px 38px 34px -26px 
`;

const Score = styled.div`
  font-size: 1.2rem;
  margin-bottom: 15px;  
`;

class AnswerResultMessage extends React.Component {

  render() {

    var resultMessage;
    if (this.props.currentCount < 1 && this.props.showTimer){
      resultMessage = <AnswerResult> {this.props.currentCount < 1 && this.props.showTimer ? 'time out. sad.' : ''}</AnswerResult>      
    } else {
      resultMessage= this.props.showResult ? 
      <AnswerResult> { this.props.hasAnsweredCorrectly ? 'Good...' : 'INCORRECT. YES. WRONG.'}</AnswerResult>
     : null;
    }
     
     const playAgainButton = this.props.hasAnsweredCorrectly !== undefined && !this.props.hasAnsweredCorrectly ? <PlayAgainButton onClick={this.props.onPlayAgain}> Play again </PlayAgainButton> 
     : null;

    return (  
      
      <Message>
        {resultMessage}
        <Score>
         Your Score: {this.props.score}
        </Score>
        {playAgainButton}
     </Message>
     
    );
  }
}


export default AnswerResultMessage;
