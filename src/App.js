import React, { Component } from 'react';
import GameBoard from './screens/GameBoard';
import styled from 'styled-components';

const Game = styled.div`
  font-family: monospace;
`;

const Title = styled.h1`
  font-family: monospace;
  margin-left: 15px;
`;

class App extends Component {
  render() {
    return (
      <Game>
        <header className="App-header">
          <Title>guess it bro</Title>
        </header>
        <GameBoard timeToAnswer='20'/>
      </Game>
    );
  }
}

export default App;
