const baseUrl = 'http://localhost:9000/users/1/facts';
const guessUrl = 'http://localhost:9000/guess';
class ApiGateway {

    getNewFacts = () => {
        return fetch(baseUrl, {
            method: 'GET', mode: 'cors'
        }).then(results => { 
            console.log(results)
            return results.json();
        })
    };

    guessCharacter = (userInputValue, characterId, userId) => {
        return fetch(guessUrl, {
            method: 'POST',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({
            answer: userInputValue,
            character_id: characterId,
            user_id: userId
            })
        }).then(results => { 
            return results.json();
        })
    };
}

let instance = null;
export default () => {
  if (instance === null) {
    instance = new ApiGateway();
  }
  return instance;
};