
import provideApiGateway from '../../infrastructure/ApiGateway.js';

const submitAnswer = ({apiGateway}) =>
(userInputValue, character, feedback, refreshFacts) => {
   
  let hasAnsweredCorrectly = userInputValue === character.name.toLowerCase();
  feedback(hasAnsweredCorrectly);
  apiGateway.guessCharacter(userInputValue, character.id, 1).then(data => {
    console.log(data)
    if (data.result === "Correct") {
      setTimeout(
        () => refreshFacts(data.characterFacts.facts, 
                      {name: data.characterFacts.character, id: data.characterFacts.id}),
        2000);
    }
  }).catch(ex => {
    console.log(ex);
    throw ex;
  });
};

const provideInstance = (specifiedDependencies) =>
submitAnswer({
    apiGateway: provideApiGateway(),
  ...specifiedDependencies});
export default provideInstance;
